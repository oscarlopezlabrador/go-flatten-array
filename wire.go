//+build wireinject

package main

import (
	"github.com/google/wire"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/command/create_flattened_array_command"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/query/find_firsts_flattened_arrays_by_id_query"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/query/find_flattened_array_by_id_query"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/infrastructure/persistence/mongo"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/infrastructure/ui/http/flattened_array_controller"
)

func InitializeFlattenedArrayRepository(connection map[string]string) domain.FlattenedArrayRepository {
	wire.Build(NewFlattenedArrayRepository)
	return mongo.FlattenArrayRepository{}
}

func NewFlattenedArrayRepository(connection map[string]string) domain.FlattenedArrayRepository {
	return mongo.New(connection["userName"], connection["password"], connection["host"], connection["port"], connection["mongoDb"])
}

func InitializeFlattenedArrayController(flattenedArrayRepository domain.FlattenedArrayRepository) flattened_array_controller.HttpController {
	wire.Build(
		flattened_array_controller.NewHttpController,
		create_flattened_array_command.NewHandler,
		find_flattened_array_by_id_query.NewHandler,
		find_firsts_flattened_arrays_by_id_query.NewHandler,
	)
	return flattened_array_controller.HttpController{}
}
