package flattened_array_controller

import (
	"fmt"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/command/create_flattened_array_command"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/query/find_firsts_flattened_arrays_by_id_query"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/query/find_flattened_array_by_id_query"
	"net/http"
)

type HttpController struct {
	createFlattenedArrayCommandHandler    create_flattened_array_command.CommandHandler
	findFlattenedArrayByIdQueryHandler    find_flattened_array_by_id_query.QueryHandler
	findFirstsFlattenedArraysQueryHandler find_firsts_flattened_arrays_by_id_query.QueryHandler
}

func NewHttpController(
	createFlattenedArrayCommand create_flattened_array_command.CommandHandler,
	findFlattenedArrayByIdQueryHandler find_flattened_array_by_id_query.QueryHandler,
	findFirstsFlattenedArraysQueryHandler find_firsts_flattened_arrays_by_id_query.QueryHandler,
) HttpController {

	return HttpController{
		createFlattenedArrayCommandHandler:    createFlattenedArrayCommand,
		findFlattenedArrayByIdQueryHandler:    findFlattenedArrayByIdQueryHandler,
		findFirstsFlattenedArraysQueryHandler: findFirstsFlattenedArraysQueryHandler,
	}
}

func (httpController HttpController) Exec(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		httpController.post(w, r)
	case "GET":
		httpController.getFirsts(w)
	}
}

func (httpController HttpController) post(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	requestId := r.Form.Get("requestId")
	arrayToBeFlattened := r.Form.Get("arrayToBeFlattened")

	error := httpController.createFlattenedArrayCommandHandler.Exec(create_flattened_array_command.Command{
		RequestId:          requestId,
		ArrayToBeFlattened: arrayToBeFlattened,
	})

	if error != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)

		fmt.Fprintf(w, error.Error())
		return
	}

	value := httpController.findFlattenedArrayByIdQueryHandler.Exec(find_flattened_array_by_id_query.Query{
		RequestId: requestId,
	})

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	fmt.Fprintf(w, value)

}

func (httpController HttpController) getFirsts(w http.ResponseWriter) {
	value := httpController.findFirstsFlattenedArraysQueryHandler.Exec(find_firsts_flattened_arrays_by_id_query.Query{})
	if value == "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, value)
}
