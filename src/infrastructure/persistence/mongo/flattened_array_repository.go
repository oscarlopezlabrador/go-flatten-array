package mongo

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type FlattenArrayRepository struct {
	client     *mongo.Client
	context    context.Context
	collection *mongo.Collection
}

func New(userName string, password string, host string, port string, database string) FlattenArrayRepository {
	flattenArrayRepository := FlattenArrayRepository{}
	flattenArrayRepository.connect(userName, password, host, port)
	flattenArrayRepository.collection = flattenArrayRepository.client.Database(database).Collection("flattened_array")
	return flattenArrayRepository
}

func (r *FlattenArrayRepository) connect(userName string, password string, host string, port string) {
	credential := options.Credential{
		Username: userName,
		Password: password,
	}
	client, err := mongo.NewClient(options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", host, port)).SetAuth(credential))
	if err != nil {
		log.Fatal(err)
		return
	}
	r.client = client

	ctx := context.Background()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
		return
	}

	r.context = ctx

	return
}

func (r FlattenArrayRepository) SaveFlattenedArray(aggregate domain.FlattenedArrayAggregate) error {
	if r.client == nil {
		return nil
	}
	_, err := r.collection.InsertOne(r.context, aggregate)
	if err != nil {
		return errors.New("aggregate duplicated")
	}
	return nil
}

func (r FlattenArrayRepository) FindFlattenedArrayById(uuid uuid.UUID) domain.FlattenedArrayAggregate {
	result := domain.FlattenedArrayAggregate{}
	filter := bson.M{"id": uuid}
	err := r.collection.FindOne(r.context, filter).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}

	return result
}

func (r FlattenArrayRepository) FindFirstsFlattenedArrays() []domain.FlattenedArrayAggregate {
	var results []domain.FlattenedArrayAggregate
	filter := bson.M{}
	cur, err := r.collection.Find(r.context, filter)
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(context.TODO()) {
		var elem domain.FlattenedArrayAggregate
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, elem)
	}
	return results
}

func (r FlattenArrayRepository) SetDbAndFixtures() {
	_, _ = r.collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: "id", Value: 1}},
			Options: options.Index().SetUnique(true),
		})
}
