package find_flattened_array_by_id_query

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
)

type QueryHandler struct {
	FlattenedArrayRepository domain.FlattenedArrayRepository
}

func NewHandler(repository domain.FlattenedArrayRepository) QueryHandler {
	return QueryHandler{FlattenedArrayRepository: repository}
}

func (queryHandler QueryHandler) Exec(query Query) string {
	flattenedArrayAsJson, _ := json.Marshal(queryHandler.FlattenedArrayRepository.FindFlattenedArrayById(uuid.MustParse(query.RequestId)))
	return string(flattenedArrayAsJson)
}
