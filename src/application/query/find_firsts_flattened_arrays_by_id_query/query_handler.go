package find_firsts_flattened_arrays_by_id_query

import (
	"encoding/json"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
)

type QueryHandler struct {
	FlattenedArrayRepository domain.FlattenedArrayRepository
}

func NewHandler(repository domain.FlattenedArrayRepository) QueryHandler {
	return QueryHandler{FlattenedArrayRepository: repository}
}

func (queryHandler QueryHandler) Exec(query Query) string {
	aggregates := queryHandler.FlattenedArrayRepository.FindFirstsFlattenedArrays()
	if len(aggregates) == 0 {
		return ""
	}
	flattenedArrayAsJson, _ := json.Marshal(aggregates)
	return string(flattenedArrayAsJson)
}
