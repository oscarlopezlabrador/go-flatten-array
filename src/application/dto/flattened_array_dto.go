package dto

import (
	"fmt"
)

func ToDto(preorderResult []interface{}) string {
	var arrayFlattened string
	arrayFlattened += "["
	for pos, v := range preorderResult {
		arrayFlattened += fmt.Sprintf("%v", v)
		if pos != len(preorderResult)-1 {
			arrayFlattened += " ; "
		}
	}
	arrayFlattened += "]"
	return arrayFlattened
}
