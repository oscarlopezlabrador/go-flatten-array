package create_flattened_array_command

import (
	"github.com/google/uuid"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/dto"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain/tree"
	"strings"
	"time"
)

type CommandHandler struct {
	FlattenedArrayRepository domain.FlattenedArrayRepository
}

func NewHandler(repository domain.FlattenedArrayRepository) CommandHandler {
	return CommandHandler{FlattenedArrayRepository: repository}
}

func (commandHandler CommandHandler) Exec(command Command) error {

	treeExample := tree.CreateTreeFromString(nil, nil, strings.ReplaceAll(command.ArrayToBeFlattened, " ", ""))
	var callbackPreOrderArr []*tree.NAryanTree

	var preorderResult []interface{}
	treeExample.PreOrderTraversal(
		treeExample,
		func(immersionTree *tree.NAryanTree) {
			callbackPreOrderArr = append(callbackPreOrderArr, immersionTree)
		},
		&preorderResult)

	arrayFlattened := dto.ToDto(preorderResult)
	aggregate := domain.FlattenedArrayAggregate{
		Id:                 uuid.MustParse(command.RequestId),
		NArianTreeAsString: strings.TrimSpace(command.ArrayToBeFlattened),
		ArrayFlattened:     arrayFlattened,
		TreeHeight:         treeExample.GetHeight(treeExample),
		DateTime:           time.Now(),
	}
	error := commandHandler.FlattenedArrayRepository.SaveFlattenedArray(aggregate)
	if error != nil {
		return error
	}
	return nil
}
