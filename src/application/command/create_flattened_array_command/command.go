package create_flattened_array_command

type Command struct {
	RequestId          string
	ArrayToBeFlattened string
}
