package domain

//go:generate mockgen -source=flattened_array_repository.go -destination=../../tests/mock/domain/flattened_array_repository_mock.go FlattenedArrayRepository

import "github.com/google/uuid"

type FlattenedArrayRepository interface {
	SaveFlattenedArray(aggregate FlattenedArrayAggregate) error
	FindFlattenedArrayById(uuid uuid.UUID) FlattenedArrayAggregate
	FindFirstsFlattenedArrays() []FlattenedArrayAggregate
	SetDbAndFixtures()
}
