package tree

import "github.com/google/uuid"

type Node struct {
	IdNode  uuid.UUID
	IsSpan  bool
	Payload interface{}
}
