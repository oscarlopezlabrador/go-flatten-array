package tree

import (
	"github.com/google/uuid"
)

type NAryanTree struct {
	Node         Node
	PreviousTree *NAryanTree
	SubTrees     []*NAryanTree
}

func CreateTreeFromString(finalTree *NAryanTree, treeToPlant *NAryanTree, expression string) *NAryanTree {

	if len(expression) == 0 {
		return finalTree
	}

	char := expression[0:1]

	if char == "[" {

		if finalTree == nil {
			finalTree = &NAryanTree{
				Node:         Node{IdNode: uuid.New(), IsSpan: true, Payload: ""},
				PreviousTree: nil,
				SubTrees:     nil,
			}
			treeToPlant = finalTree
		} else {

			tempTreeToPlant := &NAryanTree{
				Node:         Node{IdNode: uuid.New(), IsSpan: true, Payload: ""},
				PreviousTree: treeToPlant,
				SubTrees:     nil,
			}
			treeToPlant.SubTrees = append(
				treeToPlant.SubTrees,
				tempTreeToPlant)

			treeToPlant = tempTreeToPlant
		}

		return CreateTreeFromString(
			finalTree,
			treeToPlant,
			expression[1:])

	} else if char == "]" {
		if treeToPlant == nil {
			return CreateTreeFromString(
				finalTree,
				nil,
				expression[1:])
		}

		return CreateTreeFromString(
			finalTree,
			treeToPlant.PreviousTree,
			expression[1:])
	}

	if char != ";" {
		var value string
		value, expression = getValueAndShrinkExpression(expression)

		treeToPlant.SubTrees = append(
			treeToPlant.SubTrees,
			&NAryanTree{
				Node:         Node{IdNode: uuid.New(), IsSpan: false, Payload: value},
				PreviousTree: treeToPlant,
				SubTrees:     nil,
			})

		return CreateTreeFromString(
			finalTree,
			treeToPlant,
			expression)
	}

	return CreateTreeFromString(
		finalTree,
		treeToPlant,
		expression[1:])

}

func getValueAndShrinkExpression(expression string) (value string, shrunkenExpression string) {
	for pos, char := range expression {
		if char != ';' && char != ']' {
			value = value + string(char)
		} else {
			expression = expression[pos:]
			break
		}
	}
	return value, expression
}

func (nt NAryanTree) PreOrderTraversal(
	immersionTree *NAryanTree,
	callback func(immersionTree *NAryanTree),
	accumulator *[]interface{}) {

	if immersionTree == nil {
		return
	}

	if !immersionTree.Node.IsSpan {
		*accumulator = append(*accumulator, immersionTree.Node.Payload)
		nt.PreOrderTraversal(nil, nil, accumulator)
		return
	}
	callback(immersionTree)
	treePosition := 0

	for tempTree := immersionTree.getTreeByPosition(treePosition); tempTree != nil; tempTree = immersionTree.getTreeByPosition(treePosition) {
		nt.PreOrderTraversal(tempTree, callback, accumulator)
		treePosition++
	}

	if nt.GetHeight(immersionTree) > 0 {
		callback(nil)
	}

	return
}

func (nt NAryanTree) GetHeight(immersionTree *NAryanTree) int {
	if immersionTree == nil {
		return 0
	}
	max := 0
	treePosition := 0
	tempTree := immersionTree.getTreeByPosition(treePosition)

	for tempTree != nil {
		n := nt.GetHeight(tempTree) + 1
		if n > max {
			max = n
		}
		treePosition++
		tempTree = immersionTree.getTreeByPosition(treePosition)
	}

	return max
}

func (nt NAryanTree) getTreeByPosition(positionNode int) *NAryanTree {
	for treePosition, tree := range nt.SubTrees {
		if treePosition == positionNode {
			return tree
		}
	}
	return nil
}
