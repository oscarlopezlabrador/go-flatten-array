package domain

import (
	"github.com/google/uuid"
	"time"
)

type FlattenedArrayAggregate struct {
	Id                 uuid.UUID
	NArianTreeAsString string
	ArrayFlattened     string
	TreeHeight         int
	DateTime           time.Time
}
