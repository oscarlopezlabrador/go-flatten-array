module github.com/oscarlopez1616/tangueloFlattenArray

go 1.15

require (
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.1.2
	github.com/google/wire v0.4.0
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.4
)
