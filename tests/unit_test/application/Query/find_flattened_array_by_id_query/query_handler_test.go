package create_flattened_array_command

import (
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/query/find_flattened_array_by_id_query"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
	mock_domain "github.com/oscarlopez1616/tangueloFlattenArray/tests/mock/domain"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

func TestFlattenedArrayShouldBeExist(t *testing.T) {
	assert := assert.New(t)

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockFlattenedArrayRepository := mock_domain.NewMockFlattenedArrayRepository(mockCtrl)

	createdAt := time.Now()
	aggregate := domain.FlattenedArrayAggregate{
		Id:                 uuid.MustParse("bbec3a73-6ac1-4320-94dd-f7113102b619"),
		NArianTreeAsString: strings.TrimSpace("[[10 ; [[20 ; [30]]] ; [40]]]"),
		ArrayFlattened:     "[10 ; 20 ; 30 ; 40]",
		TreeHeight:         5,
		DateTime:           createdAt,
	}

	expectedFlattenedArrayAsJson, _ := json.Marshal(aggregate)
	expectedFlattenedArrayAsString := string(expectedFlattenedArrayAsJson)

	mockFlattenedArrayRepository.EXPECT().FindFlattenedArrayById(gomock.Eq(uuid.MustParse("bbec3a73-6ac1-4320-94dd-f7113102b619"))).Return(aggregate).Times(1)

	findFlattenedArrayByIdQueryHandler := find_flattened_array_by_id_query.NewHandler(mockFlattenedArrayRepository)
	value := findFlattenedArrayByIdQueryHandler.Exec(find_flattened_array_by_id_query.Query{
		RequestId: "bbec3a73-6ac1-4320-94dd-f7113102b619",
	})

	assert.Equal(expectedFlattenedArrayAsString, value, "Flattened Array Should Be Exist")
}
