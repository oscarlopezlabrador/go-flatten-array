package create_flattened_array_command

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/application/command/create_flattened_array_command"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
	mock_domain "github.com/oscarlopez1616/tangueloFlattenArray/tests/mock/domain"
	"github.com/oscarlopez1616/tangueloFlattenArray/tests/mock/match"
	"reflect"
	"strings"
	"testing"
	"time"
)

func TestFlattenedArrayShouldBeCreated(t *testing.T) {

	flattenedArrayAggregates := []domain.FlattenedArrayAggregate{
		{
			Id:                 uuid.MustParse("3162fd8f-35b7-4a90-93ab-d7fdb854b6c3"),
			NArianTreeAsString: strings.TrimSpace("[[10 ; 20 ; 30] ; 40]"),
			ArrayFlattened:     "[10 ; 20 ; 30 ; 40]",
			TreeHeight:         2,
			DateTime:           time.Now(),
		},
		{
			Id:                 uuid.MustParse("a4e45cd8-9558-41b7-afad-f7ad37edff72"),
			NArianTreeAsString: strings.TrimSpace("[[A ; 20 ; [B]] ; 40]"),
			ArrayFlattened:     "[A ; 20 ; B ; 40]",
			TreeHeight:         3,
			DateTime:           time.Now(),
		},
		{
			Id:                 uuid.MustParse("bbec3a73-6ac1-4320-94dd-f7113102b619"),
			NArianTreeAsString: strings.TrimSpace("[[10 ; [[20 ; [30]]] ; [40]]]"),
			ArrayFlattened:     "[10 ; 20 ; 30 ; 40]",
			TreeHeight:         5,
			DateTime:           time.Now(),
		},
		{
			Id:                 uuid.MustParse("c6eac25f-7e07-40bb-a515-bb5f33f95918"),
			NArianTreeAsString: strings.TrimSpace("[♣ ; ♦ ; ♥]"),
			ArrayFlattened:     "[♣ ; ♦ ; ♥]",
			TreeHeight:         1,
			DateTime:           time.Now(),
		},
	}

	for _, flattenedArrayAggregate := range flattenedArrayAggregates {
		t.Run(
			"TestFlattenedArrayShouldBeCreated",
			func(t *testing.T) {

				mockCtrl := gomock.NewController(t)
				defer mockCtrl.Finish()

				mockFlattenedArrayRepository := mock_domain.NewMockFlattenedArrayRepository(mockCtrl)

				aggregate := flattenedArrayAggregate

				mockFlattenedArrayRepository.EXPECT().SaveFlattenedArray(match.NewMatcher(
					func(arg interface{}) bool {
						s := reflect.ValueOf(arg).Interface().(domain.FlattenedArrayAggregate)
						return aggregate.Id == s.Id && aggregate.ArrayFlattened == s.ArrayFlattened && aggregate.NArianTreeAsString == s.NArianTreeAsString && aggregate.TreeHeight == s.TreeHeight
					},
				),
				).Times(1)

				createFlattenedArrayCommandHandler := create_flattened_array_command.NewHandler(mockFlattenedArrayRepository)
				createFlattenedArrayCommandHandler.Exec(create_flattened_array_command.Command{
					RequestId:          flattenedArrayAggregate.Id.String(),
					ArrayToBeFlattened: flattenedArrayAggregate.NArianTreeAsString,
				})
			})
	}
}
