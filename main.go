package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/oscarlopez1616/tangueloFlattenArray/src/domain"
	"log"
	"net/http"
	"os"
)

func main() {
	loadEnvVariables()

	serverApiPort := fmt.Sprintf(":%s", os.Getenv("SERVER_API_PORT"))

	connection := make(map[string]string)
	connection["userName"] = os.Getenv("MONGODB_USER")
	connection["password"] = os.Getenv("MONGODB_PASSWORD")
	connection["host"] = os.Getenv("MONGODB_HOST")
	connection["port"] = os.Getenv("MONGODB_PORT")
	connection["mongoDb"] = os.Getenv("MONGODB_DB")

	flattenedArrayRepository := InitializeFlattenedArrayRepository(connection)
	flattenedArrayRepository.SetDbAndFixtures()
	handleRequests(serverApiPort, flattenedArrayRepository)
}

func loadEnvVariables() {
	if os.Getenv("APP_ENV") == "" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}
}

func handleRequests(serverApiPort string, flattenedArrayRepository domain.FlattenedArrayRepository) {
	flattenedArrayController := InitializeFlattenedArrayController(flattenedArrayRepository)
	http.HandleFunc("/", flattenedArrayController.Exec)
	fmt.Println("Server has been Initialized")
	log.Fatal(http.ListenAndServe(serverApiPort, nil))
}
