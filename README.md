# To execute all unit tests
```shell
go test ./tests/unit_test/...
```

# To execute the app:

##### create .env file with the correct mongodb configuration params:
```shell
mkdir build && cp .env.dist build/.env 
```

##### To compile:
```shell
go build -o build/flattened_array_microservice
```

##### To rise up the MongoDb server:
```shell
cd etc/devTools/docker/ && docker-compose up -d
```

##### when mongodb is up execute go to build directory and execute the following:
```shell
./flattened_array_microservice
```

##### To execute a request to be flattened an array use the following command:
```shell
curl --location --request POST 'http://0.0.0.0:10000/' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'requestId=2abeeac4-cd23-4eb3-ab6e-48f0dc8c8183' --data-urlencode 'arrayToBeFlattened=[[10 ; [[20 ; [30]]] ; [40]]]'
```

##### another request :
```shell
curl --location --request POST 'http://0.0.0.0:10000/' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'requestId=e7216082-e540-4e10-a431-d863ac7476eb' --data-urlencode 'arrayToBeFlattened=[[10 ; [[80 ; [30]]] ; [40]]]'
```
##### To get last 100 request execute the following command:
```shell
curl --location --request GET 'http://0.0.0.0:10000/' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'requestId=2abeeac4-cd23-4eb3-ab6e-48f0dc8c8183' --data-urlencode 'arrayToBeFlattened=[[10 ; [[20 ; [30]]] ; [40]]]'
```

######## To regenerate the files with go generate( for now wire, and mockgen) , from project root directory execute the following command:
```shell
go generate ./...
```

######## To create mocks using mockgen tool execute the following command as example:
```shell
GO111MODULE=on mockgen -source=src/domain/flattened_array_repository.go -destination=tests/mock/domain/flattened_array_repository_mock.go
```